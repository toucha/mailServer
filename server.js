var http = require('http');
// url解析模块
var url = require('url');
// 路径解析模块
var path = require('path');
var querystring = require('querystring');

// 发送邮件
var mail = require('./postMail');

// 创建服务
http.createServer(function (req, res) {
  var reqUrl = req.url;
  var pathName = url.parse(reqUrl).pathname;
  // 对路径进行解码，防止中文乱码
  pathName = decodeURI(pathName);
  // 获取参数(get方式): params: {name: 111, age: 11}
  // var params = url.parse(reqUrl, true);

  // 获取post请求参数
  var post = '';
  // 通过req的data事件监听函数，每当接受到请求体的数据，就累加到post变量中
  req.on('data', function (chunk) {
    post += chunk;
  });
  // 在end事件触发后，通过querystring.parse将post解析为真正的POST请求格式，然后向客户端返回
  req.on('end', function () {
    post = querystring.parse(post);
    console.log(post);

    // 构造邮件对象：含发件人，收件人，标题及邮件内容
    var mailOptions = {};
    mailOptions.from = post.from;
    mailOptions.to = post.to;
    mailOptions.subject = post.subject;
    mailOptions.text = post.text;
    // 构建邮件服务的相关参数
    var transporter = {};
    transporter.service = 'qq';
    transporter.port = 465;
    transporter.auth = {
      user: 'xxxx@qq.com',
      pass: 'azcrgjxsciyccafe',
    };

    mail.postMail(mailOptions, transporter);
  })


  
  // console.log(params);
  
  res.end();

}).listen(8088);
