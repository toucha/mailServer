var nodemailer = require('nodemailer');


// mailOptions: 邮件内容，
// transporter：接收邮件服务
function postMail(mailOptions, transporter) {
  // 设置默认的邮件接收情况
  var mailDefaultOptions = {
    from: 'xxxxxxxx@qq.com', // 发件地址
    to: 'xxx@163.com', // 收件列表
    subject: 'my test 邮件发送', // 标题
    html: '<strong>这是标签内容</strong>', // 邮件格式： html 或 text
    text: '这是文本内容',
  };
  // 进行数据处理
  if (!mailOptions.from || mailOptions.from == '') {
    mailOptions.from = mailDefaultOptions.from;
  }
  if (!mailOptions.to || mailOptions.to == '') {
    return console.log('收件人不能为空！');
  }
  if (!mailOptions.subject || mailOptions.subject == '') {
    return console.log('标题不能为空！');
  }
  // 默认采用文本方式发送
  if ((!mailOptions.html || mailOptions.html == '') && (!mailOptions.text || mailOptions.text == '')) {
    mailOptions.text = mailDefaultOptions.text;
  }

  // 开启 SMTP连接
  var transporter = nodemailer.createTransport({
    service: transporter.service || 'qq',
    port: transporter.port || '465', // SMTP端口
    secureConnection: true, // 使用ssl
    // QQ邮箱 -> 设置 -> 帐户 -> 开启服务：POP3/SMTP服务()
    auth: {
      user: transporter.auth.user || 'xxxx@qq.com',
      pass: transporter.auth.pass || 'azcrgjxsciyccafe',
    }
  });

  // 发送邮件
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      return console.log(error);
    }
    console.log('邮件发送成功：' + info.response);
  });
}
module.exports.postMail= postMail;